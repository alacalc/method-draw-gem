module MethodDraw
  module FormBuilderInstanceMethods
    def method_draw(method, options = {})
      @template.render(
        :partial => "method_draw/method_draw",
        :locals => {
          :template => @template,
          :object_name => @object_name,
          :method => method,
          :options => objectify_options(options)
        }
      )
    end

    alias_method :svg_edit, :method_draw
  end

  module ActionViewBaseInstanceMethods
    def method_draw(object_name, method, options = {})
      self.render(
        :partial => "method_draw/method_draw",
        :locals => {
          :template => self,
          :object_name => object_name,
          :method => method,
          :options => options
        }
      )
    end

    alias_method :svg_edit, :method_draw
  end
end

ActionView::Helpers::FormBuilder.send :include, MethodDraw::FormBuilderInstanceMethods
ActionView::Base.send :include, MethodDraw::ActionViewBaseInstanceMethods

