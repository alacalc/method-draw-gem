module MethodDrawTag
  module ActionViewBaseInstanceMethods
    def method_draw_tag(name, value = nil, options = {})
      self.render(
        :partial => "method_draw/method_draw_tag",
        :locals => {
          :name => name,
          :id => sanitize_to_id(name),
          :value => value,
          :options => options
        }
      )
    end

    alias_method :svg_edit_tag, :method_draw_tag
  end
end

ActionView::Base.send :include, MethodDrawTag::ActionViewBaseInstanceMethods

