require 'test_helper'

module MethodDraw
  describe MethodDrawTag do
    before do
      setup_lib_spec
    end

    it 'must add methods to ActionView::Base' do
      @view.must_respond_to :method_draw_tag
    end

    it 'must show method_draw tag' do
      method_draw_tag = @view.method_draw_tag('drawing', 'svg')
      method_draw_tag.wont_be_nil
      method_draw_tag.wont_be_empty
    end
  end
end
