# MethodDraw

MethodDraw allows the easy integration of [Method-Draw](https://github.com/duopixel/Method-Draw) with any rails application.

_This Gem was adapted from the [Sketchily](https://github.com/lml/sketchily) gem._

Currently supports and provides `method-draw`.

## Installation

Add this line to your application's Gemfile:

```rb
gem 'method_draw'
```

And then execute:

```sh
$ bundle
```

Or install it yourself:

```sh
$ gem install method_draw
```

Also make sure your `application.js` includes some version of jquery. It usually does by default.
If you use the jquery-rails gem, you will have something like this:

```js
//= require jquery
//= require jquery_ujs
```

**Note:** method_draw will **automatically** add the following files to your asset precompilation list:

    method_draw.css method_draw.js method_draw_embed.js method-draw-js/canvg/canvg.js method-draw-js/canvg/rgbcolor.js method-draw-js/extensions/ext-*.js

Other html files and images used by method-draw will also be precompiled.

## Usage

MethodDraw adds new form elements which can be accessed by calling:

- `f.method_draw` or simply `method_draw` from inside a `form_for @my_object |f|`
- `method_draw_tag` from inside a `form_tag`

This gem also adds a helper method that can be called to display the resulting SVG images (without an editor):

- `method_draw_show` from any view template

### `method_draw` and `method_draw_tag` functions

- Both of these functions work exactly like the equivalent `hidden_field` functions, except that method-draw is displayed instead.
- The `f.method_draw` format accepts a method name and an options hash.
- The `method_draw` format (called without the form builder object) accepts an object name, a method name and an options hash.
- The `method_draw_tag` format accepts a tag name, the svg string to be edited and an options hash.

Currently available options are:

- `width` total width of editor
- `height` total height of editor

- `bkgd_color` canvas background color (3 or 6 hex digit html color format; not saved with image; see method_draw_show)
- `bkgd_url` canvas background image url (not saved with image; see bkgd_file option for method_draw_show)

- `canvas_width` initial canvas width
- `canvas_height` initial canvas height
- `canvas_expansion` 0 if you want to disable scrolling

- `hide_rulers` true if you want to hide the canvas rulers
- `hide_menu` true if you want method-draw's menu to be hidden
- `hide_image_tool` true if you want to hide the `image tool` button

- `show_layers` (true if you want the layer selector to display automatically when the editor is loaded)

- `url` override contents of the canvas with the svg file in the given url (careful: changes lost whenever editor is loaded)

- `id` override the default id (see explanation below)
- `value` override the default value (the svg itself; can be useful when using form_for)
- `index` override the default index (affects the tag name; can be useful when using form_for)

- `extensions` editor plugins (e.g. 'plugin.js' will load '/assets/extensions/plugin.js')

- other standard html attributes for the input tag

The `hyperlink tool` is disabled by default, as embedded links and scripts do not work with the display method used by method_draw_show.
The method_draw_show helper will display svg's inside of <img> tags, which are treated as static images by most browsers.
We consider this to be a necessary precaution when dealing with user-generated svg's.

MethodDraw requires a unique `id` (by default, this is set in the same way as hidden_field) each time it is called in the same page.
However, some uses of `form_for` can generate repeated ids (e.g. multiple `form_for @my_object.new` in the same page).
In those cases, you need to generate and specify your own unique ids. A possible solution is to use one of the many uuid generator gems.

It is recommended that the database entries associated with method_draw form elements be of type `text`.

The following examples assume that the database table for `@my_object` has a `sketch` column of type `text`.

Example usage (haml):

```haml
= form_for @my_object do |f|
  = f.method_draw :sketch, :hide_menu => true
```

### `method_draw_show` helper

- This function takes the base64-encoded SVG string as an argument and an options hash.
- The SVG string can be directly read from the field used by the `method_draw` and `method_draw_tag` functions.

Currently available options are:

- `width` width of resulting image object
- `height` height of resulting image object

- `bkgd_color` background color (3 or 6 hex digit html color format)
- `bkgd_file` background image file path (must be a local image file)

Passing only one of those options should keep the aspect ratio of the SVG constant in most browsers.

Example usage (haml):

```haml
= method_draw_show @my_object.sketch, :width => "500"
```

## Browser Support

Although more testing is needed, we currently believe method_draw supports any browsers that Method-Draw supports, namely:

- Firefox 1.5+
- Opera 9.50+
- Safari 4+
- Chrome 1+
- IE 9+
- IE 6+ (with the [Chrome Frame plugin](http://www.google.com/chromeframe)) 

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Create specs for your feature
4. Ensure that all specs pass
5. Commit your changes (`git commit -am 'Add some feature'`)
6. Push to the branch (`git push origin my-new-feature`)
7. Create new pull request
